## unstable-auto-cli

Simple Search unstable autos in jira directly from the command line

## Installation

```
npm install bitbucket:pmangialavori/unstable-autos-check-cli#master -g
```

### Put a .jirarc in your home directory (%USERPROFILE% in windows)
```
touch $HOME/.jirarc
```

and put this inside filling it up with your data

```
{
    "username": "<jira username>",
    "password": "<jira pwd>",
    "jiraOrigin": "https://jira.odigeo.com"
}
```
## Call it from cli
```
unstable-autos-cli --jsonTest='{"tests":[{"feature":"passengers_page/apply_promocodes.feature","scenarios":[{"line":279,"seed":1571821457651}]},{"feature":"register_page/register.feature","scenarios":[{"line":208,"seed":1571823047961}]},{"feature":"results_page/odirating/after_filters_applied_mobile.feature","scenarios":[{"line":72,"seed":1571823218373},{"line":49,"seed":1571823287444},{"line":88,"seed":1571823287437},{"line":61,"seed":1571823447120}]},{"feature":"results_page/odirating/ga_tracking_appearance.feature","scenarios":[{"line":199,"seed":1571823250129},{"line":262,"seed":1571823356489},{"line":60,"seed":1571823310645},{"line":200,"seed":1571823310480},{"line":59,"seed":1571823389956},{"line":196,"seed":1571823414002},{"line":162,"seed":1571823429017},{"line":198,"seed":1571823469970},{"line":235,"seed":1571823476787}]},{"feature":"results_page/odirating/header_checks.feature","scenarios":[{"line":294,"seed":1571823334828}]},{"feature":"results_page/results_page_pagination_and_select.feature","scenarios":[{"line":84,"seed":1571823509913}]}]}'
```

You should see somenthing like this:
```
1 / 6 failing tests are unstable. Total known unstable 23
--------------------------------------------------------------------------------
--- Failed Tests that are unstable are marked with v --- '
--------------------------------------------------------------------------------
KO, passengers_page/apply_promocodes.feature, x
KO, results_page/odirating/after_filters_applied_mobile.feature, x
KO, results_page/odirating/ga_tracking_appearance.feature, x
KO, results_page/odirating/header_checks.feature, x
KO, results_page/results_page_pagination_and_select.feature, x
OUA-306, register_page/register.feature, v
```

If not make a PR ;)
### If you have a streaming VDI you may need this
```
sudo echo /etc/hosts >> 185.8.141.220 jira.odigeo.com
```