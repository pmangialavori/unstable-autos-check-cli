import fs from 'fs';
import path from 'path';

const fileName = '.jirarc';


export function getHomePath() {
  return process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'];
}

export function readConfigInDir(dir) {
  return fs.readFileSync(path.join(dir, fileName), 'utf8');
}

export function getConfigFile() {
  try {
    return readConfigInDir(process.cwd());
  } catch (err) {
    return readConfigInDir(getHomePath());
  }
}

export function getConfig() {
  let config = null;

  try {
    const file = getConfigFile();

    try {
      config = JSON.parse(file);
    } catch (err) {
      console.log(`invalid json in ${fileName}`);
    }
  } catch (err) {
    // console.log('no rc');
  }

  return config;
}
