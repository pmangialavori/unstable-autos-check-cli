import args from 'yargs';
import chalk from 'chalk';
import inquirer from 'inquirer';
import { jql } from './JQLRest.mjs';
import { JQL_UNSTABLE_AUTOS } from './JQLQueries.mjs';
import pkg from '../package.json';
const log = console.log.bind(console);
const { argv } = args
  .usage('Usage: unstable-autos <command> [options]')
  .command('unstable-autos', 'Retrieve the unstable autos from jira and filter out with given test')
  .example('unstable-autos --jsonTest={"tests":[]}', 'Retrieve the unstable autos from jira and filter out with given test')
  .alias('t', 'jsonTest')  
  .describe('t', 'The result of jenkins autos execution')
  .demandOption(['t'])
  .help('h')
  .alias('h', 'help')
  .epilog(`by ${pkg.author}`);
/**
 * ask input to user
 * @returns {promise}
 */
function askFilter() {
  const questions = [
    {
      type: 'input',
      name: 'filter',
      message: 'Enter a simple text to filter it:',
      filter: function(val) {
        return val;
      },
      validate: function(value) {
        return value !== "";
      },
      //default: 'xxxx'
    },
  ];
  return inquirer.prompt(questions);
}

/**
 *
 *
 * @param {*} test
 */
function renderTest(test) {
  log(`${test.isUnstable ? chalk.green(test.key) : chalk.red(`KO`)}, ${test.feature}, ${test.isUnstable ? chalk.green('v') : chalk.red('x')}`);
}


function parseFile(data) {
  try {
    return JSON.parse(data);
  } catch(e) {
    return data;
  }
}

export async function main() {
  try {
    const { issues } = await jql({ jql: JQL_UNSTABLE_AUTOS }).then(_ => _.json());
  
    const { tests } = parseFile(argv.jsonTest);
  
    for(let test of tests) {
      for(let issue of issues) {
        if (issue.fields.summary.indexOf(test.feature) > -1) {
          // the test is unstable
          test.isUnstable = true;
          test.key = issue.key;
        }
      }
    }  
    const sorted = tests.sort(test => test.isUnstable);
    log(`${chalk.green(sorted.filter(test => test.isUnstable).length)} / ${chalk.green(sorted.length)} failing tests are unstable. Total known unstable ${chalk.green(issues.length)}`);
    log(`${"-".repeat(80)}`);
    log(`--- Failed Tests that are unstable are marked with ${chalk.green('v')} --- '`);
    log(`${"-".repeat(80)}`);
    sorted.map(renderTest);
    process.exit(0);

  } catch(err) {
    log(err);
    process.exit(1);
  }
}