
import fetch from 'node-fetch';
import { getConfig } from './config.mjs';
export const JIRA_ENDPOINT = '/rest/api/2';

/**
 *
 *
 * @param {String} username
 * @param {String} password
 * @returns
 */
function encodeBasicAuth(username, password) {
    return Buffer.from(`${username}:${password}`).toString('base64');    
}

/**
 * generate headers for request
 *
 * @param {*} { username, password }
 * @returns
 */
function createHeaders({ username, password }) {
    return {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeBasicAuth(username, password)}`,
    }
}

/**
 *
 *
 * @export
 * @param {*} [{ jql, auth }={}]
 * @returns
 */
export async function jql({ jql } = {}) {
    const { jiraOrigin, username, password } = getConfig();    
    return fetch(`${jiraOrigin}${JIRA_ENDPOINT}/search`, { 
            headers: createHeaders({ username, password }), 
            method: 'POST', 
            body: JSON.stringify({ jql })
        } 
    );
}